class Person():
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f'Person {self.name} and age {self.age} year old'

    def __repr__(self):
        return f"<{self.name} and age {self.age}"


veer = Person('veer', 24)

print(veer.age)
print(veer)
