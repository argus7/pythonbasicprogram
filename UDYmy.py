# String formatting

name = "Ravi"
greeting = "hello, {}"
with_name = greeting.format("Veer")
print(with_name)


new_name = "Veer"
day = "Monday"
new_greeting = "Hell0 {} , Today is {}"

formatted_string = new_greeting.format(new_name, day)
print(formatted_string)